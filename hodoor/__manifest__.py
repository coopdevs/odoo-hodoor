# -*- coding: utf-8 -*-
{
    'name': "Hodoor",
    'summary': """""",
    'description': """""",
    'author': "Coopdevs Treball SCCL",
    'website': "https://gitlab.com/coopdevs/odoo-hodoor",
    'version': '0.1',
    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'product'
    ],
    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
